Feature: This feature describes....

	@Completed 
	Scenario Outline: This scenario is for...
  	Given that the system is in a certain state...
  	When some action is performed using <PARAM1>, <PARAM2> and <PARAM3>
  	Then the system stores a message <PARAM1>, <PARAM2>, <PARAM3> with status 'SUCCESSFUL'
  	Examples:
  	|PARAM1|PARAM2|PARAM3|
	|value1|value2|value3|
	
	@Inprogress
	Scenario Outline: This scenario is for...
  	Given that the system is in a certain state...
  	When some action is performed using <PARAM1>, <PARAM2> and <PARAM3>
  	Then the system stores a message <PARAM1>, <PARAM2>, <PARAM3> with status 'FAILED'
  	Examples:
  	|PARAM1|PARAM2|PARAM3|
	|value1|value2|value3|